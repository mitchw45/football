# Initialize conda in shell:
source $(conda config --show root_prefix | cut -d: -f2)/etc/profile.d/conda.sh 
# Get the active environment:
RLDM_ENV="$(conda info | grep -o 'active env location :.*' | cut -d: -f2)"
echo $RLDM_ENV

if test -z "$RLDM_ENV"; then
  echo "\$RLDM_ENV is empty, activating rldm_p3 environment..."
  conda activate rldm_p3
  RLDM_ENV="$(conda info | grep -o 'active env location :.*' | cut -d: -f2)"
elif [[ $RLDM_ENV != *"rldm_p3"* ]]; then
  echo "Warning: another conda enviroment is active.  Activating rldm_p3..."
  conda activate rldm_p3
  RLDM_ENV="$(conda info | grep -o 'active env location :.*' | cut -d: -f2)"
else
  echo "\$RLDM_ENV = ${RLDM_ENV}"
fi

if test -z "$RLDM_ENV"; then
  echo "Unable to activate rldm_p3 environment in conda, ensure the conda step succeeded before running this script."
  exit 1
fi
# source ~/miniconda3/etc/profile.d/conda.sh

rm third_party/gfootball_engine/CMakeCache.txt
rm third_party/gfootball_engine/CMakeFiles/cmake.check_cache
pip install .
cd $RLDM_ENV/lib/python3.7/site-packages/gfootball_engine
echo "path: ${RLDM_ENV}"

rm CMakeCache.txt
rm CMakeFiles/cmake.check_cache

rm _gameplayfootball.so
cmake . && make -j `nproc`
ln -s libgame.so _gameplayfootball.so
rm -f -- ${RLDM_ENV}/lib/libstdc++.so.6
