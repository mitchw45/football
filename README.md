# Google Research Football - RLDM P3

This repository contains an RL environment based on open-source game Gameplay
Football. <br> It was created by the Google Brain team for research purposes. 

This repository is hosted to support a native install of the environment for RLDM P3. It contains an additional environment.yml file
to create a conda environment. It also provides a script to install gfootball from source, but using the python packages that the conda 
environment uses. 

This works on PopOS 22.04 and Arch yay. I have not tested it on other Linux distributions.

### 1. Install required packages
#### Debian/Ubuntu Linux
```shell
sudo apt-get install git cmake build-essential libgl1-mesa-dev libsdl2-dev \
libsdl2-image-dev libsdl2-ttf-dev libsdl2-gfx-dev libboost-all-dev \
libdirectfb-dev libst-dev mesa-utils xvfb x11vnc python3-pip

python3 -m pip install --upgrade pip setuptools psutil wheel
```
#### Arch Linux
```shell
paru -S sdl2 sdl2_image sdl2_ttf sdl2_gfx

python -m pip install --upgrade pip setuptools psutil wheel
```

#### Conda
Install conda using the directions at https://conda.io/projects/conda/en/latest/user-guide/install/index.html

### 2. Verify CMAKE version
Run `cmake --version` and check that it is 3.22.x. This is required so that CMAKE works properly with conda. If it is not, update it using your package manager.

### 3. Install environment using conda
```shell
cd football
conda env create --file environment.yml
```

### 4. Install gfootball using this repository
```shell
chmod +x modified_install.sh
./modified_install.sh
```

### 5. Test Installation
Do the below steps from another directory e.g. your P3 working directory, otherwise this folder will conflict with the installed version gfootball that we created in step 4.

Activate the environment: `conda activate rldm_p3`

Start a python shell: `python`

Import the gfootball module: `import gfootball`


If there are no errors or warnings, the environment should be working.

